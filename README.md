# Git User

## Demo Project

[Demo Project](https://gituser-two.vercel.app/)

## Short description

Проект предоставляет собой реализацию тестового задания на вакансию Junior Frontend Developer

## Project Technologies

- TypeScript
- React
- React Router
- Redux/Redux ToolKit
- RTK Query
- Styled Components
- Vite

## Install Project

```
    yarn
    yarn dev
```
