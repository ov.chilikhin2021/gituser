import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Layout from '@/layouts/Layout/Layout';
import Home from '@/pages/Home/Home';

const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route path="/" element={<Home />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};
export default Router;
