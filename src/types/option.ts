export type Option = {
  label: string;
  selected?: boolean;
  value: string;
};
