import { Outlet } from 'react-router-dom';

import logo from '@/assets/logo.svg';

import { Footer, Header, Logo, LogoImg, Main } from './Layout.styled';

const Layout = () => {
  return (
    <>
      <Header>
        <Logo to="/">
          <LogoImg src={logo} alt="" />
          Git Users
        </Logo>
      </Header>
      <Main>
        <Outlet />
      </Main>
      <Footer>&copy; Copyright 2023</Footer>
    </>
  );
};
export default Layout;
