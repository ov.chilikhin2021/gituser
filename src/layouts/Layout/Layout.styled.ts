import { Link } from 'react-router-dom';
import { styled } from 'styled-components';

export const Header = styled.header`
  padding: 0 3em;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.3);
  width: 100%;
  height: 80px;
  display: flex;
  align-items: center;
`;

export const Logo = styled(Link)`
  display: flex;
  align-items: center;
  color: #000;
  gap: 1em;
  position: relative;
  height: 50%;
  font-size: 1.5em;
`;

export const LogoImg = styled.img`
  height: 100%;
`;

export const Main = styled.main`
  margin: 2em 0;
  padding: 0 3em;
  flex: 1 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 2em;
`;

export const Footer = styled.footer`
  box-shadow: 0 -5px 10px rgba(0, 0, 0, 0.3);
  height: 50px;
  padding: 0 3em;
  flex-shrink: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 1.15em;
`;
