import { Global } from 'App.styled';
import { Provider } from 'react-redux';

import Router from './routers/router';
import { store } from './store/store';

const App = () => {
  return (
    <>
      <Provider store={store}>
        <Global />
        <Router />
      </Provider>
    </>
  );
};
export default App;
