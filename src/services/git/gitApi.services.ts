import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';

import { Responses, UserInfo } from '@/types/response';
import { PER_PAGE } from '@/utils/constaint';

export const gitApi = createApi({
  reducerPath: 'git',
  baseQuery: fetchBaseQuery({
    baseUrl: 'https://api.github.com',
  }),

  endpoints: (build) => ({
    searchUsers: build.query<Responses, { search: string; page: number; order: string }>({
      query: (args) => {
        const { search, page, order } = args;
        const searchWithArgs = `${search}+in:login`;
        return {
          url: `/search/users?q=${searchWithArgs}`,
          params: {
            per_page: PER_PAGE,
            page: page,
            sort: 'repositories',
            order: order,
          },
        };
      },
    }),

    getUser: build.query<UserInfo, string>({
      query: (username) => {
        return {
          url: `/users/${username}`,
        };
      },
    }),
  }),
});

export const { useSearchUsersQuery, useLazyGetUserQuery } = gitApi;
