export const MAX_COUNT = 1000;

export const PER_PAGE = 10;

export const SIBLING_COUNT = 2;
