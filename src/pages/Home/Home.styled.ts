import styled from 'styled-components';

export const SearchBox = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 1em;
`;
