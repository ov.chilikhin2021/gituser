import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import ProfileList from '@/components/ProfileList/ProfileList';
import Search from '@/components/Search/Search';
import { useSearchUsersQuery } from '@/services/git/gitApi.services';
import {
  setCurrentPage,
  setTotalCount,
} from '@/store/features/pagination/paginationSlice';
import { RootState } from '@/store/store';

import { SearchBox } from './Home.styled';

const Home = () => {
  const { search } = useSelector((state: RootState) => state.search);
  const { order } = useSelector((state: RootState) => state.order);
  const { currentPage: page } = useSelector((state: RootState) => state.pagination);

  const dispatch = useDispatch();

  const { data, isLoading, isError } = useSearchUsersQuery(
    { search, page, order },
    {
      skip: search.length === 0,
    },
  );

  useEffect(() => {
    dispatch(setCurrentPage(1));
  }, [search]);

  useEffect(() => {
    if (!data) return;
    const totalCount = data.total_count;
    dispatch(setTotalCount(totalCount));
  }, [data]);
  return (
    <>
      <SearchBox>
        <Search />
      </SearchBox>

      {isLoading && <p>Loading</p>}
      {isError && <p>Something went wrong</p>}
      {data?.items?.length === 0 && search !== '' ? <p>Users not found</p> : ''}
      {search !== '' && !isError ? <ProfileList data={data?.items} /> : ''}
    </>
  );
};
export default Home;
