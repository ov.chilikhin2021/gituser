import App from 'App';
import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';

const root = document.querySelector('#root') as HTMLElement;

createRoot(root).render(
  <StrictMode>
    <App />
  </StrictMode>,
);
