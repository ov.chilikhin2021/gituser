import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState = {
  order: '',
};

const orderSlice = createSlice({
  name: 'search',
  initialState,
  reducers: {
    setOrder(state, action: PayloadAction<string>) {
      return {
        ...state,
        order: action.payload,
      };
    },
  },
});

export const { setOrder } = orderSlice.actions;

export default orderSlice.reducer;
