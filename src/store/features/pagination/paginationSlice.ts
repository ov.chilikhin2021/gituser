import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { MAX_COUNT, PER_PAGE } from '@/utils/constaint';

const initialState = {
  currentPage: 1,
  totalCount: MAX_COUNT,
  totalPages: 1,
};

const paginationSlice = createSlice({
  name: 'pagination',
  initialState,
  reducers: {
    setCurrentPage(state, action: PayloadAction<number>) {
      return {
        ...state,
        currentPage: action.payload,
      };
    },
    setTotalCount(state, action: PayloadAction<number>) {
      const totalCount = action.payload >= MAX_COUNT ? MAX_COUNT : action.payload;
      const totalPages = Math.ceil(totalCount / PER_PAGE);

      return {
        ...state,
        totalCount,
        totalPages,
      };
    },
  },
});

export const { setCurrentPage, setTotalCount } = paginationSlice.actions;

export default paginationSlice.reducer;
