import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/dist/query';

import { gitApi } from '@/services/git/gitApi.services';

import orderReducer from './features/order/orderSlice';
import paginationReducer from './features/pagination/paginationSlice';
import searchReducer from './features/search/searchSlice';

export const store = configureStore({
  reducer: {
    search: searchReducer,
    order: orderReducer,
    pagination: paginationReducer,
    [gitApi.reducerPath]: gitApi.reducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(gitApi.middleware),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
setupListeners(store.dispatch);
