import { User } from '@/types/response';

import Pagination from '../Pagination/Pagination';
import ProfileItem from '../ProfileItem/ProfileItem';
import { List } from './ProfileList.styled';

type ProfileListProps = {
  data: User[] | undefined;
};

const ProfileList = ({ data }: ProfileListProps) => {
  return (
    <>
      <List>{data?.map((item) => <ProfileItem key={item.id} item={item} />)}</List>
      {data && <Pagination />}
    </>
  );
};
export default ProfileList;
