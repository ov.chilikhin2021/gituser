import { IoMdArrowDropdown } from 'react-icons/io';
import styled from 'styled-components';

export const SelectContainer = styled.div`
  width: 150px;
  height: 63px;
  position: relative;
  cursor: pointer;
  border-bottom: 1px solid #000;
`;

export const SelectOutput = styled.div`
  height: 100%;
  position: relative;
  width: auto;
  display: flex;
  align-items: center;
  padding: 0 0.5em;
`;

export const Icon = styled(IoMdArrowDropdown)`
  position: absolute;
  right: 0;

  &.active {
    transform: rotate(180deg);
  }
`;

export const OptionContainer = styled.ul`
  position: absolute;
  border-radius: 5px;
  display: flex;
  flex-direction: column;
  z-index: 1;

  top: 73px;
  width: 150px;

  background: #c3c3c3;
`;

export const OptionElement = styled.li`
  padding: 0 10px;
  display: flex;
  align-items: center;

  width: 100%;
  height: 30px;
  border-radius: 5px;
  transition: background 0.3s ease-in-out;

  &:hover {
    background: #d3d3d3;
  }
  &.selected {
    border-right: 2px solid #000;
  }
`;
