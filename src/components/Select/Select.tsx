import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import { setOrder } from '@/store/features/order/orderSlice';
import { Option } from '@/types/option';

import {
  Icon,
  OptionContainer,
  OptionElement,
  SelectContainer,
  SelectOutput,
} from './Select,styled';

type SelectType = {
  defaultValue?: string;
  options: Option[];
};

const Select = ({ defaultValue, options }: SelectType) => {
  const [isActive, setIsActive] = useState(false);
  const selectedOption = options.filter((option) => option.selected === true)[0];
  const [selected, setSelected] = useState(selectedOption);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setOrder(selected.value));
  }, [selected]);

  return (
    <>
      <SelectContainer
        onClick={() => {
          setIsActive((prev) => !prev);
        }}
      >
        <SelectOutput>
          <span>{selected ? selected.label : defaultValue}</span>
          <Icon className={isActive ? 'active' : ''} />
        </SelectOutput>

        {isActive && (
          <OptionContainer>
            {options.map((option, idx) => (
              <OptionElement
                className={selected.label === option.label ? 'selected' : ''}
                onClick={() => {
                  setSelected(option);
                }}
                key={idx}
              >
                {option.label}
              </OptionElement>
            ))}
          </OptionContainer>
        )}
      </SelectContainer>
    </>
  );
};
export default Select;
