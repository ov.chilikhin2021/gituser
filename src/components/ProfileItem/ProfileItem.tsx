import { useEffect, useState } from 'react';
import { PiUsersThreeBold } from 'react-icons/pi';
import { VscGist, VscRepo } from 'react-icons/vsc';

import { useLazyGetUserQuery } from '@/services/git/gitApi.services';
import { User } from '@/types/response';

import { Image, Info, Item, ItemInfo, ItemPreview, Login } from './ProfileItem.styled';

type ProfileItemType = {
  item: User;
};

const ProfileItem = ({ item }: ProfileItemType) => {
  const [isActive, setIsActive] = useState(false);

  const [getUser, { data }] = useLazyGetUserQuery();

  useEffect(() => {
    if (!isActive) return;

    getUser(item.login);
  }, [isActive]);

  const handleClick = () => {
    setIsActive((prev) => !prev);
  };

  return (
    <Item className={isActive ? 'active' : ''} onClick={handleClick}>
      <ItemPreview>
        <Image src={item.avatar_url} alt={item.avatar_url} />
        <Login href={item.html_url} target="_blank" rel="noreferrer">
          {item.login}
        </Login>
      </ItemPreview>
      {isActive && (
        <ItemInfo>
          <Info>
            <VscRepo />
            {data?.public_repos}
          </Info>
          <Info>
            <PiUsersThreeBold />
            {data?.followers}
          </Info>
          <Info>
            <VscGist /> {data?.public_gists}
          </Info>
        </ItemInfo>
      )}
    </Item>
  );
};
export default ProfileItem;
