import styled from 'styled-components';

export const Item = styled.li`
  display: flex;
  flex-direction: column;
  gap: 2em;
  width: 500px;
  height: 100px;
  padding: 1em;
  position: relative;
  background: #fff;
  transition:
    height 0.4s ease-in-out,
    box-shadow 0.4s ease-in-out;

  cursor: pointer;
  border-radius: 1em;

  &.active {
    height: 160px;
  }

  &:hover {
    box-shadow: 10px 7px 7px #d3a3f3;
  }
`;

export const ItemPreview = styled.section`
  display: flex;
  align-items: center;
  gap: 2em;
`;

export const Image = styled.img`
  width: 75px;
  border-radius: 50%;
  height: auto;
`;

export const Login = styled.a`
  font-size: 1.4em;
  color: #000;

  transition: color 0.4s ease-in-out;

  &:hover {
    color: #0000dd;
  }
`;

export const ItemInfo = styled.section`
  display: flex;
  height: 40px;
  align-items: center;
  gap: 1em;
`;

export const Info = styled.span`
  display: flex;
  gap: 0.5em;
  align-items: center;
  font-size: 1.5em;
`;
