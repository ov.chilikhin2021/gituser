import { useDispatch, useSelector } from 'react-redux';

import { DOTS, usePagination } from '@/hooks/usePagination';
import { setCurrentPage } from '@/store/features/pagination/paginationSlice';
import { RootState } from '@/store/store';
import { SIBLING_COUNT } from '@/utils/constaint';

import { Container, Item } from './Pagination.styled';

const Pagination = () => {
  const { currentPage, totalPages } = useSelector((state: RootState) => state.pagination);
  const siblingCount = SIBLING_COUNT;
  const paginationRange = usePagination({
    currentPage,
    totalPageCount: totalPages,
    siblingCount,
  });
  const lastPage = paginationRange[paginationRange.length - 1];
  const dispatch = useDispatch();

  const onPrevPage = () => {
    dispatch(setCurrentPage(currentPage - 1));
  };
  const onNextPage = () => {
    dispatch(setCurrentPage(currentPage + 1));
  };

  const onPageChange = (pageNumber: string | number) => {
    const isNumber = typeof pageNumber === 'number';

    if (!isNumber) return;

    dispatch(setCurrentPage(pageNumber));
  };

  if (currentPage === 0 || paginationRange.length < 2) {
    return null;
  }
  return (
    <Container>
      {/* Left navigation arrow */}
      <Item onClick={onPrevPage} className={currentPage === 1 ? 'disabled' : ''}>
        <div className="arrow left" />
      </Item>
      {paginationRange.map((pageNumber, idx) => {
        // If the pageItem is a DOT, render the DOTS unicode character
        if (pageNumber === DOTS) {
          return (
            <Item key={idx} className="dots">
              &#8230;
            </Item>
          );
        }

        // Render our Page Pills
        return (
          <Item
            key={idx}
            className={pageNumber === currentPage ? 'selected' : ''}
            onClick={() => onPageChange(pageNumber)}
          >
            {pageNumber}
          </Item>
        );
      })}
      {/*  Right Navigation arrow */}
      <Item className={currentPage === lastPage ? 'disabled' : ''} onClick={onNextPage}>
        <div className="arrow right" />
      </Item>
    </Container>
  );
};
export default Pagination;
