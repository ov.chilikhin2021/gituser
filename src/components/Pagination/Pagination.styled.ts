import styled from 'styled-components';

export const Container = styled.ul`
  display: flex;
  align-items: center;
`;

export const Item = styled.li`
  padding: 0 12px;
  height: 2em;
  text-align: center;
  margin: auto 4px;
  color: rgba(0, 0, 0, 0.87);
  display: flex;
  box-sizing: border-box;
  align-items: center;
  justify-content: center;
  letter-spacing: 0.01071em;
  border-radius: 50%;
  line-height: 1.43;
  font-size: 1.5em;
  min-width: 2em;

  &.dots:hover {
    background-color: transparent;
    cursor: default;
  }

  &:hover {
    background-color: rgba(0, 0, 0, 0.2);
    cursor: pointer;
  }

  &.selected {
    background-color: rgba(0, 0, 0, 0.15);
  }

  &.disabled {
    pointer-events: none;
  }

  .arrow {
    &::before {
      position: relative;
      /* top: 3pt; Uncomment this to lower the icons as requested in comments*/
      content: '';
      /* By using an em scale, the arrows will size with the font */
      display: inline-block;
      width: 0.5em;
      height: 0.5em;
      border-right: 0.15em solid rgba(0, 0, 0, 0.87);
      border-top: 0.15em solid rgba(0, 0, 0, 0.87);
    }

    &.left {
      transform: rotate(-135deg) translate(-50%);
    }

    &.right {
      transform: rotate(45deg);
    }
  }
`;
