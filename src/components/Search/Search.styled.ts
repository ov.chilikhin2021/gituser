import styled from 'styled-components';

export const Input = styled.input`
  border-bottom: 1px solid #222;
  padding: 20px 10px;
  width: 320px;
  font-size: 1.2em;
`;
