import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import { useDebounce } from '@/hooks/useDebounce';
import { setSearch } from '@/store/features/search/searchSlice';
import { Option } from '@/types/option';

import Select from '../Select/Select';
import { Input } from './Search.styled';

const Search = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const debounced = useDebounce(searchTerm, 500);
  const dispatch = useDispatch();

  const optionsSelect: Option[] = [
    { value: 'asc', label: 'По возрастанию' },
    { value: 'desc', label: 'По убыванию', selected: true },
  ];

  useEffect(() => {
    dispatch(setSearch(debounced));
  }, [debounced]);

  return (
    <>
      <Input
        type="text"
        placeholder="Enter Login"
        value={searchTerm}
        onChange={(e) => {
          setSearchTerm(e.target.value);
        }}
      />

      <Select defaultValue="Выберите сортировку" options={optionsSelect} />
    </>
  );
};
export default Search;
